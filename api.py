import aiohttp
import urllib.parse

import config
from config import VIA_HOST, VIA_SCHEME


class ApiError(Exception):
    pass


class BadRequestError(ApiError):
    pass


class PermissionDeniedError(ApiError):
    pass


class NotFoundError(ApiError):
    pass


class InternalServerError(ApiError):
    pass


class UnauthorizedError(ApiError):
    pass


def build_url(path, query_args=None):
    query = urllib.parse.urlencode(query_args if query_args else {})
    parse_result = urllib.parse.ParseResult(
        scheme=VIA_SCHEME, netloc=VIA_HOST, path=path,
        params='', query=query, fragment='')
    return urllib.parse.urlunparse(parse_result)


def check_status(status):
    if status == 400:
        raise BadRequestError
    elif status == 401:
        raise UnauthorizedError
    elif status == 403:
        raise PermissionDeniedError
    elif status == 404:
        raise NotFoundError
    elif status == 500:
        raise InternalServerError


async def get_json(url, token=None):
    headers = {'Authorization': f'Bearer {token}'} if token else {}
    async with aiohttp.ClientSession() as session:
        async with session.get(url, headers=headers) as resp:
            print(f"== [{resp.status}] {url}")
            check_status(resp.status)
            return await resp.json()


async def post_json(url, obj, token=None):
    headers = {'Authorization': f'Bearer {token}'} if token else {}
    async with aiohttp.ClientSession() as session:
        async with session.post(url, json=obj, headers=headers) as resp:
            print(f"== [{resp.status}] {url}")
            check_status(resp.status)
            return await resp.json()


async def post_form(url, obj):
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    async with aiohttp.ClientSession() as session:
        async with session.post(url, data=obj, headers=headers) as resp:
            print(f"== [{resp.status}] {url}")
            check_status(resp.status)
            return await resp.json()


async def patch_json(url, obj, token=None):
    headers = {'Authorization': f'Bearer {token}'} if token else {}
    async with aiohttp.ClientSession() as session:
        async with session.patch(url, json=obj, headers=headers) as resp:
            print(f"== [{resp.status}] {url}")
            check_status(resp.status)
            return await resp.json()


async def test_token(token):
    url = build_url('/oauth/introspect')
    data = {
        "token": token,
        "client_id": config.VIA_CLIENT_ID,
        "client_secret": config.VIA_CLIENT_SECRET
    }
    return await post_form(url, data)


async def get_user_groups(token, user_id):
    url = build_url(f'/api/users/{user_id:d}/groups/')
    return await get_json(url, token)


async def get_tasks_current_user(token):
    url = build_url('/api/tasks/', {})
    return await get_json(url, token=token)


async def get_group_tasks(token, group_id):
    url = build_url(f'/api/groups/{group_id:d}/tasks/')
    return await get_json(url, token=token)


async def get_group(token, group_id):
    url = build_url(f'/api/groups/{group_id:d}/')
    return await get_json(url, token=token)


async def add_group_task(token, group_id, owners, title):
    url = build_url(f'/api/tasks/')
    obj = {
        "title": title,
        "group_id": group_id,
        "users": [*owners.split(' ')]
    }
    return await post_json(url, obj, token=token)


async def get_group_users(token, group_id):
    """Retrieve all users from viaduct by transversing pagination."""
    page = 1

    def url():
        return build_url(f'/api/groups/{group_id:d}/users/?page={page:d}')

    resp = await get_json(url(), token=token)
    users = resp['data']
    while resp['page'] < resp['page_count']:
        page = resp['page'] + 1
        resp = await get_json(url(), token=token)
        users.extend(resp['data'])
    return users


async def get_task(token, task_id):
    url = build_url(f'/api/tasks/{task_id:s}/')
    return await get_json(url, token=token)


async def set_task_status(token, task_id, status):
    url = build_url(f'/api/tasks/{task_id:s}/')
    return await patch_json(url, {'status': status}, token=token)


async def get_access_tokens(code, bot_username):
    """Get access token from viaduct using authorization code."""
    scheme = config.VIA_SCHEME
    host = config.VIA_HOST
    client_id = config.VIA_CLIENT_ID
    client_secret = config.VIA_CLIENT_SECRET
    url = build_url('/oauth/token')

    data = {
        "grant_type": "authorization_code",
        "client_id": client_id,
        "client_secret": client_secret,
        "code": code,
        # "state": state,
        'redirect_uri': f"{scheme}://{host}/telegram/{bot_username}/redirect/"
    }
    return await post_form(url, data)


async def refresh_token(token):
    """Get new access_token from viaduct using refresh token."""
    url = build_url('/oauth/token')
    data = {
        "grant_type": "refresh_token",
        # state:{{state}}
        "refresh_token": token,
        "client_id": config.VIA_CLIENT_ID,
        "client_secret": config.VIA_CLIENT_SECRET
    }
    return await post_form(url, data)
