from sqlalchemy import Column, String, Integer, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from typing import Optional

database_file = 'database.sqlite'
engine = create_engine(f'sqlite:///{database_file}')
Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()


class Token(Base):
    __tablename__ = 'tokens'

    id = Column(Integer, primary_key=True)
    telegram_user_id = Column(String(64))
    access_token = Column(String(64))
    refresh_token = Column(String(64))

    @classmethod
    def get_token(cls, telegram_user_id: str) -> Optional['Token']:
        return session.query(cls) \
            .filter(cls.telegram_user_id == telegram_user_id) \
            .one_or_none()

    def save(self):
        session.add(self)
        session.commit()


class Group(Base):
    __tablename__ = 'groups'

    id = Column(Integer, primary_key=True)
    telegram_chat_id = Column(String(64))
    viaduct_group_id = Column(Integer)

    @classmethod
    def get_group(cls, telegram_chat_id: str) -> Optional['Group']:
        return session.query(cls) \
            .filter(cls.telegram_chat_id == telegram_chat_id) \
            .one_or_none()

    def save(self):
        session.add(self)
        session.commit()


Base.metadata.create_all(bind=engine)
