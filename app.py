#!/usr/bin/env python3
from collections import defaultdict

import json
import re
from functools import wraps

import api
import messages
import telewalrus.bot
from callbacks import callback_tasks, callback_set_group, callback_status
from config import TG_TOKEN
from database import Token, Group
from utils import get_valid_token_from_database, get_task_from_args

BOT = telewalrus.bot.Bot(TG_TOKEN)


def check_token(f):
    @wraps(f)
    async def wrapper(message: telewalrus.bot.Message):
        token = await get_valid_token_from_database(message.from_user.id)

        if not token:
            name = message.from_user.first_name
            bot_username = message.bot.ownuser.username

            if message.chat.type == 'private':
                msg = messages.authenticate_message(name, bot_username)
            else:
                msg = messages.stranger_message(name, bot_username)
            return await message.chat.message(msg, parse_mode='HTML',
                                              disable_web_page_preview=True)
        return await f(token.access_token, message)
    return wrapper


@BOT.command('help')
@check_token
async def cmd_help(_, message):
    return await message.chat.message(messages.help_message())


@BOT.command('start')
async def cmd_start(message):
    name = message.from_user.first_name
    token: Token = Token.get_token(str(message.from_user.id))
    if token:
        await message.chat.message(
            f'Welkom back, {name}! Use /help to see my commands.')
        return

    code = message.args.strip()
    bot_username = message.bot.ownuser.username

    if not code:
        await message.chat.message(
            messages.authenticate_message(name, bot_username),
            parse_mode='HTML',
            disable_web_page_preview=True)
        return

    try:
        new_token: dict = await api.get_access_tokens(code, bot_username)
    except api.BadRequestError:
        return await message.chat.message(
            f'Hey, {name}! The provided authorization code is not valid.')

    token: Token = Token(telegram_user_id=str(message.from_user.id),
                         access_token=new_token.get('access_token'),
                         refresh_token=new_token.get('refresh_token'))
    token.save()

    await message.chat.message(
        f'Welcome, {name}! Use /help to see my commands.')


@BOT.command('config')
@check_token
async def cmd_config(token, message):
    token_details = await api.test_token(token)

    if message.chat.type == 'private':
        msg = messages.config_user_message(token_details.get('username'))
        return await message.chat.message(msg, parse_mode='HTML')
    else:
        group: Group = Group.get_group(str(message.chat.id))
        if group:
            via_group = await api.get_group(token, group.viaduct_group_id)
            msg = messages.config_group_message(via_group.get('name'), True)
        else:
            msg = messages.config_group_message(None, True)

        groups = await api.get_user_groups(token, token_details.get('sub'))
        keyboard = []
        for i, group in enumerate(groups):
            if i % 3 == 0:
                keyboard.append([])

            keyboard[-1].append({
                'text': f"{group['name']}",
                'callback_data': f'group {group["id"]}'
            })

        reply_markup = {'inline_keyboard': keyboard}

        await message.chat.message(msg, parse_mode='HTML',
                                   disable_web_page_preview=True,
                                   reply_markup=json.dumps(reply_markup))


@BOT.command('chatinfo')
async def cmd_chatinfo(message):
    chat = message.chat

    if message.chat.type == 'private':
        admins = []
    else:
        admins = [admin.user for admin in await chat.administrators()]

    msg = f'''
id: {chat.id}
type: {chat.type}
title: {chat.title}
username: {chat.username}
first_name: {chat.first_name}
last_name: {chat.last_name}
admins: {admins}
    '''
    await message.chat.message(msg)


@BOT.command('tasks')
@check_token
async def cmd_tasks(token, message):
    is_private = message.chat.type == 'private'
    if is_private:
        tasks = await api.get_tasks_current_user(token)
        if not tasks:
            await message.chat.message('You have no tasks')
            return

        group_names = {}
        for group_id in {task['group_id'] for task in tasks}:
            via_group = await api.get_group(token, group_id)
            group_names[group_id] = via_group['name']

        msg = messages.user_tasks_message(tasks, group_names)
        await message.chat.message(msg, parse_mode='HTML')
    else:
        group: Group = Group.get_group(str(message.chat.id))
        if group is None:
            msg = messages.config_group_required_message()
            return await message.chat.message(msg)

        return await cmd_grouptasks(token, message, group.viaduct_group_id)


async def cmd_grouptasks(token, message, group_id):
    group_tasks = await api.get_group_tasks(token, group_id)

    msg = ''
    user_task_status = defaultdict(lambda: defaultdict(int))

    for i, task in enumerate(group_tasks):
        for user in task['users']:
            user_task_status[user][task['status']] += 1

    for user_id, status in user_task_status.items():
        msg += f'<b>{user_id}</b>:\n' \
               f'    ⏸ {status["new"]}, ▶️ {status["started"]}, ' \
               f'✅ {status["done"]}, ❌ {status["remove"]}\n'

    users = await api.get_group_users(token, group_id)

    keyboard = [[{'text': f"Everyone", 'callback_data': f'tasks 0 Everyone'}]]
    for i, user in enumerate(users):
        if i % 3 == 0:
            keyboard.append([])

        keyboard[-1].append({
            'text': f"{user['first_name']} {user['last_name']}",
            'callback_data': f'tasks {user["id"]} {user["first_name"]}'
        })

    msg += '\nClick a name to view his/her tasks.'
    reply_markup = {'inline_keyboard': keyboard}

    await message.chat.message(msg, parse_mode='HTML',
                               disable_web_page_preview=True,
                               reply_markup=json.dumps(reply_markup))


@BOT.command('task')
@check_token
async def cmd_task(token, message):
    if message.chat.type != 'private':
        group: Group = Group.get_group(str(message.chat.id))
        if not group:
            msg = messages.config_group_required_message()
            return await message.chat.message(msg)

    task, _ = await get_task_from_args(token, message)
    if not task:
        return

    msg, reply_markup = messages.task_message(task)

    await message.chat.message(msg, parse_mode='HTML',
                               reply_markup=json.dumps(reply_markup),
                               disable_web_page_preview=True)


@BOT.command('new')
@check_token
async def cmd_actie(token, message):
    if message.chat.type == 'private':
        await message.chat.message(
            'Creating tasks is only supported in groups.')
        return

    group: Group = Group.get_group(str(message.chat.id))
    if not group:
        msg = messages.config_group_required_message()
        return await message.chat.message(msg)

    match = re.match(r'^([^:]+): (.*)$', message.args)
    if not match:
        match = re.match(r'^([^ ]+) (.*)$', message.args)
        if match:
            owner = match.group(1)
            title = match.group(2)
            await message.chat.message(
                f'Incorrect syntax. Did you mean /new {owner}: {title}?')
        else:
            await message.chat.message(
                f'Incorrect syntax. Use /new <name>: <title>.')

        return

    task = await api.add_group_task(
        token, group.viaduct_group_id, match.group(1), match.group(2))
    msg, reply_markup = messages.task_message(task)
    msg = f'Task <code>[{task["b32_id"]}]</code> created.\n\n' + msg

    await message.chat.message(msg, parse_mode='HTML',
                               reply_markup=json.dumps(reply_markup),
                               disable_web_page_preview=True)


@BOT.callback
async def callback(query):
    command, *args = query.data.split(' ')
    handler = {
        'status': callback_status,
        'tasks': callback_tasks,
        'group': callback_set_group
    }.get(command)
    if handler:
        await handler(query, command, args)


while True:
    try:
        BOT.run()
    except KeyboardInterrupt:
        print('\nSaving database/')
        break
    except IOError:
        pass
