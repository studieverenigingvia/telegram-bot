import api
from database import Token


async def get_valid_token_from_database(telegram_user_id: str):
    token = Token.get_token(telegram_user_id)
    if not token:
        return
    valid = await api.test_token(token.access_token)
    if not valid or not valid.get('active'):
        new_token: dict = await api.refresh_token(token.refresh_token)
        token.access_token = new_token.get('access_token')
        token.refresh_token = new_token.get('refresh_token')
        token.save()
    return token


async def get_task_from_args(token, message):
    task_id = message.args
    if not task_id:
        await message.chat.message(
            "Please specify task_id using: '/task <task_id>'")
        return

    try:
        task = await api.get_task(token, task_id)
    except api.NotFoundError:
        await message.chat.message(f"Task '{task_id}' not found.")
        return
    except api.PermissionDeniedError:
        await message.chat.message(
            f"You have no rights for task '{task_id}'.")
        return

    return task, task_id