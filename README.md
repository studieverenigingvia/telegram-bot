# Studieverening via bot
Make sure to clone recursively:
`git clone --recursive git@gitlab.com:studieverenigingvia/telegram-bot.git`.
If you forgot, just run `git submodule update --init` after cloning.

## Set-up
Copy `config.py.example` to `config.py`, and edit it as the comments say.

Then, create a virtualenv (or not), then run `pip3 install -r requirements.txt`

*Note: pimpybot requires Python 3.6. It will not work with 3.7.*


## Running
`python3 app.py`.

# Bot Father
Send the @BotFather the following descriptions from /help to initiate the bots commands.