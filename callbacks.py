from functools import wraps

import json

import api
import messages
from utils import get_valid_token_from_database
from database import Group


def check_callback_token(f):
    @wraps(f)
    async def wrapper(query, _, args):
        """Get the token from the database."""

        await query.answer()
        token = await get_valid_token_from_database(str(query.from_user.id))

        if not token:
            msg = messages.stranger_message(
                query.message.from_user.first_name,
                query.message.bot.ownuser.username)
            await query.message.edit(msg)
        else:
            await f(token.access_token, query, args)

    return wrapper


@check_callback_token
async def callback_tasks(token, query, args):
    group = Group.get_group(str(query.message.chat.id))
    if not group:
        return

    via_group = await api.get_group(token, group.viaduct_group_id)
    tasks = await api.get_group_tasks(token, group.viaduct_group_id)

    user_name = None

    if int(args[0]) != 0:
        user_name = ' '.join(args[1:])
        tasks = list(filter(lambda task: user_name in ' '.join(task['users']),
                            tasks))

    # tasks = await api.get_group_user_tasks(token, group_id, user_id)
    msg = messages.group_tasks_message(tasks,
                                       group_name=via_group['name'],
                                       user_name=user_name)
    await query.message.edit(msg, parse_mode='HTML',
                             disable_web_page_preview=True)


@check_callback_token
async def callback_set_group(token, query, args):
    admin_ids = {admin.user.id for admin in
                 await query.message.chat.administrators()}
    if query.from_user.id not in admin_ids:
        return await query.message.chat.message(
            "Sorry! Only chat admins can set group.")

    viaduct_group_id = int(args[0])
    telegram_chat_id = str(query.message.chat.id)
    group = Group.get_group(telegram_chat_id=telegram_chat_id)

    # Get group or create new one.
    if group:
        group.viaduct_group_id = viaduct_group_id
    else:
        group = Group(telegram_chat_id=telegram_chat_id,
                      viaduct_group_id=viaduct_group_id)
    group.save()

    try:
        group = await api.get_group(token, group.viaduct_group_id)
    except api.PermissionDeniedError:
        msg = f'You can only set group which you are a member of.'
    else:
        msg = messages.config_group_message(group.get('name'), False)
    await query.message.edit(msg, parse_mode='HTML')


@check_callback_token
async def callback_status(token, query, args):
    status, task_id = args

    await api.set_task_status(token, task_id, status)
    task = await api.get_task(token, task_id)

    msg, reply_markup = messages.task_message(task)
    await query.message.edit(msg, parse_mode='HTML',
                             reply_markup=json.dumps(reply_markup),
                             disable_web_page_preview=True)