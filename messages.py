from collections import defaultdict

import random
from datetime import datetime
# locale.setlocale(locale.LC_TIME, 'nl_NL')
from typing import List, Optional

import config

STATUS_EMOJI = {
    'new': '⏸',
    'started': '▶️',
    'done': '✅',
    'remove': '❌'
}

TASK_STATUS = {
    'new': "Not started",
    'started': "Started",
    'done': "Done",
    'remove': "Remove",
}


def help_message():
    return f'''
/help - Show this overview
/new - Create new task. Available only in groups.
/task - List details of task.
/tasks - List all tasks for group/you.
/config - Show details about your/group configuration.
'''


def stranger_message(name, bot_username):
    """Send message for unknown users in group."""
    return f'''
Hey, {name}! It seems I don't know you!
Please get in touch with me (@{bot_username}), so we can configure your access token.
'''


def authenticate_message(name, bot_username):
    """Send message for new user with link to OAuth authorize."""
    scheme = config.VIA_SCHEME
    host = config.VIA_HOST
    client_id = config.VIA_CLIENT_ID
    redir_url = f"{scheme}://{host}/telegram/{bot_username}/redirect/"
    state = random.randint(0, 1e10)

    url = f'{scheme}://{host}/oauth/authorize?response_type=code' \
          f'&client_id={client_id}&redirect_uri={redir_url}' \
          f'&scope=pimpy+group+user' \
          f'&state={state}'
    print(f"== {url}")
    return f'Hey, {name}!\nTo get started you need to authenticate with ' \
           f'your <strong>via</strong>-user.\nPlease log-in using your' \
           f' via account <a href="{url}">here</a>.\nWhen you are redirected' \
           f' back, hit the start button again!'


def config_user_message(email):
    return f'Your telegram account is currently linked with' \
           f' <strong>via</strong>-account: {email}'


def config_group_message(group_name: Optional[str], buttons: bool):
    if group_name is not None:
        msg = f'This telegram chat is currently linked to' \
              f' <strong>via</strong>-group: {group_name}'
    else:
        msg = f'This telegram chat is currently not linked to' \
              f' any <strong>via</strong>-group.'
    if buttons:
        msg += f' Link this group to any of your groups.'
    return msg


def config_group_required_message():
    return 'The bot has not been configured for this group.\n' \
           'Use /config as chat admin to configure the bot.'


def task_info_message(random_task_id):
    return f'Use /task &lt;task_id&gt; for more details. ' \
           f'Example: /task {random_task_id}'


def user_tasks_message(tasks, group_names: dict):
    if not len(tasks):
        return f'You have no tasks.'

    groups = defaultdict(list)
    for task in tasks:
        groups[task['group_id']].append(task)

    msg = f'<strong>Your tasks:</strong>\n\n'

    for group_id, group_tasks in groups.items():
        msg += taskset_message(group_tasks, group_names[group_id])

    random_task = random.choice(tasks)['b32_id']
    return msg + task_info_message(random_task)


def group_tasks_message(tasks, group_name: str,
                        user_name: Optional[str] = None):
    """Create response for list of tasks for a group."""
    if not tasks:
        if user_name is None:
            return f'There are no tasks for {group_name}.'
        else:
            return f'There are no tasks for {user_name} in {group_name}.'

    users = defaultdict(list)
    for task in tasks:
        for user in task['users']:
            if task not in users[user]:
                users[user].append(task)

    msg = f'<strong>{group_name} tasks for this group:</strong>\n\n'

    for user_name, user_tasks in users.items():
        msg += taskset_message(user_tasks, user_name)

    random_task = random.choice(tasks)['b32_id']
    return msg + task_info_message(random_task)


def taskset_message(tasks: List[dict], name: str = ''):
    msg = f'<strong>{name}:</strong>\n' if name else ''
    for task in tasks:
        task_code = task['b32_id']
        emoji = STATUS_EMOJI[task['status']]
        if len(task['users']) == 2:
            emoji += ' 👨‍👦'
        elif len(task['users']) == 3:
            emoji += ' 👨‍👧‍👦'
        elif len(task['users']) > 3:
            emoji += ' 👨‍👩‍👧‍👧'

        msg += f'• <code>[{task_code}]</code> ' \
               f'{emoji} {task["title"].strip()}'

        msg += '\n'

    msg += '\n'

    return msg


def task_message(task):
    task_code = task['b32_id']
    msg = f'<code>[{task_code}]</code> <strong>{task["title"]}</strong>\n'

    try:
        timestamp = datetime.strptime(task["created"], "%Y-%m-%dT%H:%M:%S.%f")
    except ValueError:
        try:
            timestamp = datetime.strptime(task["created"], "%Y-%m-%dT%H:%M:%S")
        except ValueError:
            return None

    msg += f'<em>{timestamp.strftime("%d %B %Y, %H:%M")}</em>\n\n'

    # Print the task group
    msg += f'<strong>Group:</strong> {task["group_id"]}\n'

    # Print the task state
    task_status = TASK_STATUS[task["status"]]
    msg += f'<strong>Status:</strong> {task_status}\n'

    # Print task owner(s)
    users = task['users']
    if not users:
        msg += f'<em>No owners</em>\n'
    elif len(users) == 1:
        msg += f'<strong>Owner:</strong> {users[0]}\n'
    elif 1 < len(users) <= 2:
        msg += f'<strong>Owners:</strong> ' \
               f'{users[0]} and {users[1]}\n'
    else:
        msg += '\n<strong>Owners:</strong>\n'
        for user in task['users']:
            msg += f'• {user}\n'
    msg += '\n'

    # Print the description, if available
    if task["content"]:
        msg += f'<strong>Description:</strong>\n{task["content"]}\n\n'

    # Print the minute URL, if available
    try:
        minute_id = task['minute_id']
        minute_url = f'http://svia.nl/pimpy/minutes/single/{minute_id}/'
        minute_url += str(task['line'])

        msg += f'<a href="{minute_url}">View minute</a>\n'
    except KeyError:
        msg += f'<em>No minutes</em>\n'

    keyboard = []
    if task['status'] != 'new':
        keyboard.append({
            'text': '⏸ Not started',
            'callback_data': f'status new {task["b32_id"]}'
        })
    if task['status'] != 'started':
        keyboard.append({
            'text': '▶️ Started',
            'callback_data': f'status started {task["b32_id"]}'
        })
    if task['status'] != 'done':
        keyboard.append({
            'text': '✅ Done',
            'callback_data': f'status done {task["b32_id"]}'
        })
    if task['status'] != 'remove':
        keyboard.append({
            'text': '❌ Not Done',
            'callback_data': f'status remove {task["b32_id"]}'
        })

    reply_markup = {'inline_keyboard': [keyboard]}
    return msg, reply_markup
